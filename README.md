# Archtic: dotfiles

Managed using [dotbare](https://github.com/kazhala/dotbare).

Download and install these dotfiles using the following command:

```
dotbare finit -u https://gitlab.com/archtic/dot.git 
```
