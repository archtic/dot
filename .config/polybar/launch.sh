#!/bin/bash

killall -p polybar

polybar main 2>&1 | tee -a /tmp/polybar.log & disown
