" automatically install missing plugins
" note: vim-plug is installed by setup.sh
"autocmd VimEnter * if empty($GIT_DIR) && len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
"    \| PlugInstall --sync | source $MYVIMRC
"\| endif

" plugins
call plug#begin('.config/nvim/plugged')

" interface
Plug 'crispgm/nvim-tabline'

" colorization
Plug 'norcalli/nvim-colorizer.lua'
Plug 'luochen1990/rainbow'

" file browsing
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" code completion / lang servers
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'omnisharp/omnisharp-vim'

call plug#end()

" nvim-tabline
:lua require('tabline').setup({})
" nvim-colorizer
set termguicolors
lua require 'plug-colorizer'

" rainbow
let g:rainbow_active = 1

" coc
let g:coc_global_config="$HOME/.config/coc/coc-settings.json"
let g:coc_global_extensions=[
    \"coc-tsserver",
    \"coc-angular",
    \"coc-json",
    \"coc-eslint",
\]

" omnisharp
let g:OmniSharp_server_stdio = 1
