export ZSH="$HOME/.oh-my-zsh"

. ~/.zsh_aliases

ZSH_THEME="robbyrussell"
plugins=(
	git
	dotbare
	fzf 
	fzf-tab
	sudo
	zsh-autosuggestions
	zsh-syntax-highlighting
)

export PATH=$PATH:~/bin:$HOME/.dotnet/tools

export NNN_FIFO="/tmp/nnn.fifo"
export NNN_PLUG="p:-preview-tui"
export DMENU_OPTIONS=(-fn 'CaskaydiaCove Nerd Font:size=24' -nb '#111111' -nf '#EEEEEE' -sb '#EEEEEE' -sf '#111111')

export FZF_BASE="/usr/bin/fzf"
export FZF_DEFAULT_COMMAND='ag -g "" --hidden'
export FZF_CTRL_T_COMMAND='ag -g "" --hidden'

eval "$(zoxide init zsh)"

source $ZSH/oh-my-zsh.sh

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
